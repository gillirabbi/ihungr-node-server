# ihungr-node-server
Heroku + GitHub integration has been configured for this repo.
Heroku will automatically build and release pushes to this repo.
The node application is accessible at https://ihungr-node-server.herokuapp.com/

#API
* GET / - returns all running sessions
* GET /api/session/remove - deletes all sessions
* GET /api/session/:id - returns session info by id
* GET /api/restaurants - returns all restaurants
* GET /api/restaurants/:id - returns restaurant by id
* POST /api/location - Updates location of user,  requires session_id, fb_id, longitude, latitude
* POST /api/hungry - Finds a buddy, requires id, name, friends
