'use strict';

const express       = require('express');
const bodyParser    = require('body-parser');
const restaurants   = require('./restaurants');
const Parse         = require("parse/node").Parse;
const mongoose      = require ("mongoose");
const ObjectID      = require('mongodb').ObjectID;
const models        = require('./models');

const app = express();

// Here we find an appropriate database to connect to, defaulting to
// localhost if we don't find one.
const dbURI = process.env.MONGOLAB_URI || process.env.MONGOHQ_URL || 'mongodb://localhost/HelloMongoose';
app.set('port', (process.env.PORT || 5000));

Parse.initialize(
    "nPnaSc0ycVHK70P5h5w9JmJ504kMiXm1nQTag6O5", // applicationId
    "ek1wCYS7O8w8i3NcJyvsvQJL3x4zhzvICLiCwYsN", // javaScriptKey
    "yHG86LNrEgE3edvBq98CLaO3iHZS9RP2xqQ1Yi56" // masterKey
  );

let hungryUsers = [];
let usersFriends = [];
let getRestaurant;

app.get('/', (req, res) => {
  models.Session.find({}, function(err, docs) {
    if (err) {
      return res.status(500).send('Error 500: Server error');
    } else {
      return res.send(docs);
    }
  });
});

//returns restaurants
app.get('/api/restaurants', (req, res) => {
    res.send(restaurants);
});

//returns restaurant by id
app.get('/api/restaurants/:id', (req, res) => {
    const id = req.params.id;
    res.send(restaurants[id]);
});

//Removes all sessions info
app.get('/api/session/remove', (req, res) => {
  models.Session.remove({}, (err) => {
    if (err) {
      console.error(err);
      res.status(500).send('Server ERROR: db error');
    } else {
        console.log('session cleared');
      res.send('ok');
    }
  });
});

//Returns session info
app.get('/api/session/:id', (req, res) => {
  const id = req.params.id;
  models.Session.findOne({_id: new ObjectID(id)}, (err, docs) => {
    if (err) {
      return res.status(500).send('Error 500: Server error');
    } else {
      return res.send(docs);
    }
  });
});

//Returns restaurant for session
app.get('/api/session/:id/restaurant', (req, res) => {
  const id = req.params.id;
  models.Session.findOne({_id: new ObjectID(id)}, (err, docs) => {
    if (err) {
      return res.status(500).send('Error 500: Server error');
    } else {
      if(docs){
        return res.send(restaurants[docs.restaurant]);
      }
      return res.status(404).send('Session not found');
    }
  });
});

//Updates location of user with current location
app.post('/api/location', bodyParser.json(), (req, res) => {
    if(!req.body.hasOwnProperty('session_id') ||
      !req.body.hasOwnProperty('fb_id') || 
      !req.body.hasOwnProperty('longitude') ||
      !req.body.hasOwnProperty('latitude')) {
        console.log('Bad request');
        return res.status(412).send('Error 412: Precondition failed');
    }
    const data = req.body;

    let query = { '_id': new ObjectID(data.session_id) };
    let sessionUsers;
    models.Session.findOne(query, (err, doc) => {
        if(err) {
            return res.status(500).send('Server error');
        } else if (!doc) {  // check for null
            return res.status(404).send('Not found');
        } else {
            sessionUsers = doc.users;
            let userFound = false;
            // Find the user we want to update
            for(let u of sessionUsers) {
                if(u.facebookID === data.fb_id) {
                    u.latitude = data.latitude;
                    u.longitude = data.longitude;
                    userFound = true;
                    break;
                }
            }

            if(!userFound) {
                return res.status(404).send('User not found');
            }

            // Update the session
            models.Session.update({ _id: doc._id }, { $set: {users: sessionUsers} }, { upsert: true }, (ierr, idocs) => {
                if (ierr) {
                    console.error(ierr);
                    return res.status(500).send('Server ERROR: error when updating session');
                } else {
                    //console.log(idocs);
                    return res.status(201).send({'session_id': doc._id});
                }
            });
        }
    });

});

// Let the server know that you are hungry!
// send a list of your fb friends so the server can match
app.post('/api/hungry', bodyParser.json(), (req, res) => {
    console.log("hungry!");
    //console.log(req.body);
    const data = req.body;
    if(data === undefined) {
        console.log('Bad request');
        return res.status(400).send('Bad request');
    }

    //pick randomly restaurant
    let randomRestaurantIndex = getRestaurant(data.foodChoices);
    let restaurantDTO = {
        name: restaurants[randomRestaurantIndex].name,
        longitude: restaurants[randomRestaurantIndex].longitude,
        latitude: restaurants[randomRestaurantIndex].latitude
    };
    //console.log('Pref, restaurant sent to client', restaurants[randomRestaurantIndex]);

    //create session
    const session = new models.Session({users: [], restaurant: randomRestaurantIndex});
    session.save((err, doc) => {
      if (err) {
        return res.status(500).send('Server ERROR: error when creating session');
      } else {
        const sessionUsers = [];
        //create session users
        sessionUsers.push(new models.SessionUser({SessionID: doc._id, facebookID: data.id, name: data.name, longitude: data.longitude, latitude: data.latitude }));
        for (let i = 0; i < data.friends.length; i++){
          sessionUsers.push(new models.SessionUser({SessionID: doc._id, facebookID: data.friends[i].id, name: data.friends[i].name, longitude: data.friends[i].longitude, latitude: data.friends[i].latitude}));
        }
        //add users to session
        models.Session.update({ _id: doc._id }, { $set: {users: sessionUsers} }, { upsert: true }, (ierr, idocs) => {
          if (ierr) {
            console.error(ierr);
            return res.status(500).send('Server ERROR: error when creating session');
          } else {
            console.log(idocs);
            res.status(201).send({'session_id': doc._id, 'restaurant': restaurantDTO});
          }
        });

        //send notification
        let query = new Parse.Query(Parse.Installation);

        Parse.Push.send({
          where: query,
          data: {
            alert: data.name + ' is hungry!',
            session_id: doc._id
          }
        }, {
          success: function() {
            // Push was successful
            console.log('success');
            //res.status(200).send(doc._id);
          },
          error: function(error) {
            // Handle error
            console.error(error);
            //res.status(502).send('PARSE-ERROR');
          }
        });
      }
    });
});

// Additional functions
let getRandomArbitrary = function(min, max) {
    return Math.floor(Math.random() * ((max - 1) - min + 1)) + min;
};

getRestaurant = (pref) => {
    console.log('Pref from client:', pref);
    if(pref === undefined) {
        return 0;
    } else {
        if(pref.length === undefined || pref.length === 0) {
            return 0;
        }
        console.log('Pref, random index:', pref[getRandomArbitrary(0, pref.length)]);
        switch (pref[0]) {
            case 'Pizza':
                return 2;
            case 'Hamburger':
                return 3;
            case 'Hot dog':
                return 1;
            case 'Sushi':
                return 1;
            case 'Subs':
                return 2;
            case 'Chicken':
                return 0;

            default:
                return 0;
        }
    }
};

// Makes connection asynchronously.  Mongoose will queue up database
// operations and release them when the connection is complete.
mongoose.connect(dbURI, function (err, res) {
  if (err) {
  console.log ('ERROR connecting to: ' + dbURI + '. ' + err);
  } else {
  console.log ('Succeeded connected to: ' + dbURI);
  app.listen(app.get('port'), function() {
    console.log('Node app is running on port', app.get('port'));
  });
  }
});
