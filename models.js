'use strict';

const mongoose = require('mongoose');

//User
const UserSchema = mongoose.Schema({
  facebookID: {
    type: String
  },
  name: {
    type: String
  }
});

//Data for user in session
const SessionUserSchema = mongoose.Schema({
  SessionID: {
    type: String
  },
  name: {
    type: String
  },
  facebookID: {
    type: String
  },
  longitude: {
    type: String,
    default: -21.938016
  },
  latitude: {
    type: String,
    default: 64.138812
  }
});

//Food session
const SessionSchema = mongoose.Schema({
  users: [SessionUserSchema],
  restaurant: {
    type: Number
  }
});

module.exports = {
  User: mongoose.model('User', UserSchema),
  SessionUser: mongoose.model('SessionUser', SessionUserSchema),
  Session: mongoose.model('Session', SessionSchema),
};
